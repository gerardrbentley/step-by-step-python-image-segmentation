# Step by Step Python Image Segmentation

Ground up guide to image segmentation using Convolutional Neural Networks using Python. Main packages used are Pytorch, OpenCV, PIL / Pillow, and Jupyter / Google Colab Notebooks for easy demos.

Motivating Example is segmenting UI elements from screenshots of Pokemon games.

A conda environment yaml file is available for quick install of necessary components (warning, opencv is rather large, using google colab may be easier if you're limited on resources)
