"""
Note: it's not great practice to keep all of this in the same file. This is for demo use in notebook mainly.
Personally I usually keep the following:
  - pytorch_models file or directory
  - datasets.py file for custom dataset implementations
  - custom_transforms.py file for simultaneous input-target transforms 
"""
from argparse import Namespace
import imageio
import os
import glob
import logging as log
import json
import time
import datetime
from collections import namedtuple
from typing import Tuple
import argparse

import PIL
from PIL import Image

import torch
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F

import torchvision

import pytorch_lightning
print(f"torch {torch.__version__}, torchvision {torchvision.__version__}, PIL {PIL.__version__}, pytorch-lightning {pytorch_lightning.__version__} loaded")

# Holds a single image and segmentation target pair
Sample = namedtuple('Sample', ['image', 'target'])


class UIImageMaskDataset(torch.utils.data.Dataset):
    """
    Expects images to be .pngs in data_dir folder of form `1_input.png`, target ui masks to be .jpg files with same number as image and target `1_target.jpg`
    Initialize
    Param: 
    data_dir: (String) path to directory with images and targets
    transform: Torchvision transform to be applied on each image & target pair when fetched from the dataset
    overfit_num: (int) number of times to repeat the first image in the folder as the entire dataset. Leave blank or 0 to disable. Set to a postive integer to enable.
    """

    def __init__(self, data_dir='./test_inputs', transform=None, overfit_num=0):
        self.data_dir = data_dir

        # Get file paths
        # We could use a more clear wildcard (like *_input.png and *_target.jpg if we set up our filenames like that).
        self.image_list = glob.glob(f'{self.data_dir}/*.png')
        self.target_list = glob.glob(f'{self.data_dir}/*.jpg')

        # For use in methods
        self.transform = transform
        self.length = len(self.image_list)
        if overfit_num > 0:
            self.length = overfit_num
            self.image = Image.open(self.image_list[0]).convert('RGB')
            self.target = Image.open(self.target_list[0]).convert('1')
        else:
            self.image = None
            self.target = None

        # Basic Error checking, just for testing
        if self.transform is None:
            print('No transform on data, you probably need at least ToTensor()')
        if len(self.image_list) != len(self.target_list):
            print('Image list and Target list not same length')

    def __len__(self):
        """
        Required by Pytorch. Return the amount of samples. 
        (For overfitting this function returns a fixed number. 
        It can even made infinite with generators, but that's an advanced topic)
        """
        return self.length

    def __getitem__(self, idx):
        """
        Also required by Pytorch. 
        Loads input and target
        Do transformation if necessary
        Return a `sample` / `item`, meaning an input and target pair.
        If overfit_num was set above 0 will always return the same image and target.
        For efficiency usually you do the actual loading here and not in the __init__.
            The Dataloader uses multiple processes to call __getitem__
            (With larger amounts of RAM you can preload images)
        Return type and format will vary from person to person, I prefer using a NamedTuple. 
            I find this to be the most compatible with the Dataloader default Collation,
            also it's readable: you can access with `data.image` and `data.target` instead of `data[0]` and `data[1]` (which you can also still do)
        Other Return types are typically plain tuple (usually input first, target second), and dictionary
            dictionary and namedtuple allow you to expand your targets for doing more complex tasks (e.x. bounding boxes and segmentations and class labels)
            without having tuples with many numbered elements
        """
        if self.image is None:
            screenshot_file = self.image_list[idx]
            target_file = self.target_list[idx]

            image = Image.open(screenshot_file).convert('RGB')
            target = Image.open(target_file).convert('1')
        else:
            image = self.image
            target = self.target

        if self.transform:
            image, target = self.transform(image, target)

        # sample = {'image': image, 'target': target}
        return Sample(image, target)


class ImageTargetToTensor(object):
    def __call__(self, image, target):
        # Transform Classes built with __call__ act kind of like a function once you assign an instance to a variable
        tv_tt = torchvision.transforms.ToTensor()
        # transform image to tensor with torchvision transform
        image = tv_tt(image)
        # same with target
        target = tv_tt(target)
        return (image, target)


class ImageTargetResize(object):
    def __init__(self, size):
        """
        size: int will reduce shorter side to size param
             tuple of (int, int) will resize to (height, width) from size param
        returns tuple of (resized_image, resized_target)
        """
        self.size = size

    def __call__(self, image, target):
        resize_transform = torchvision.transforms.Resize(self.size)
        image = resize_transform(image)
        target = resize_transform(target)
        return (image, target)


class ImageTargetCompose(object):
    def __init__(self, transforms):
        """
        transforms: list of transforms that take in image and target and return tuples of (transformed_image, transformed_target)
        returns tuple of (image, target) after going through each transform in list
        """
        self.transforms = transforms

    def __call__(self, image, target):
        for t in self.transforms:
            image, target = t(image, target)
        return image, target


class UNetDownConvBlock(nn.Module):
    r"""Performs two 3x3 2D convolutions, each followed by a ReLU and batch norm. Ends with a 2D max-pool operation."""

    def __init__(self, in_features: int, out_features: int):
        r"""
        :param in_features: Number of feature channels in the incoming data
        :param out_features: Number of feature channels in the outgoing data
        """
        super(UNetDownConvBlock, self).__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_features, out_features, 3),
            nn.ReLU(),
            nn.BatchNorm2d(out_features),
            nn.ReplicationPad2d(1),
            nn.Conv2d(out_features, out_features, 3),
            nn.ReLU(),
            nn.BatchNorm2d(out_features),
            nn.ReplicationPad2d(1),
        )
        self.pool = nn.MaxPool2d(2)

    def forward(self, x: Tensor) -> Tensor:
        """Pushes a set of inputs (x) through the network.

        :param x: Input values
        :return: Module outputs
        """
        feature_map = self.layers(x)
        return self.pool(feature_map), feature_map


class UNetUpConvBlock(nn.Module):
    r"""Performs two 3x3 2D convolutions, each followed by a ReLU and batch norm. Ends with a transposed convolution with a stride of 2 on the last layer. Halves features at first and third convolutions"""

    def __init__(self, in_features: int, mid_features: int, out_features: int):
        r"""
        :param in_features: Number of feature channels in the incoming data
        :param out_features: Number of feature channels in the outgoing data
        """
        super(UNetUpConvBlock, self).__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_features, mid_features, 3),
            nn.BatchNorm2d(mid_features),
            nn.ReLU(),
            nn.ReplicationPad2d(1),
            nn.Conv2d(mid_features, mid_features, 3),
            nn.BatchNorm2d(mid_features),
            nn.ReLU(),
            nn.ReplicationPad2d(1),
            nn.ConvTranspose2d(mid_features, out_features, 2, stride=2),
        )

    def forward(self, x: Tensor) -> Tensor:
        """Pushes a set of inputs (x) through the network.

        :param x: Input values
        :return: Module outputs
        """
        return self.layers(x)


class UNetOutputBlock(nn.Module):
    r"""Performs two 3x3 2D convolutions, each followed by a ReLU and batch Norm.
    Ending with a 1x1 convolution to map features to classes."""

    def __init__(self, in_features: int, num_classes: int):
        r"""
        :param in_features: Number of feature channels in the incoming data
        :param num_classes: Number of feature channels in the outgoing data
        """
        super(UNetOutputBlock, self).__init__()
        mid_features = int(in_features / 2)
        self.layers = nn.Sequential(
            nn.Conv2d(in_features, mid_features, 3),
            nn.BatchNorm2d(mid_features),
            nn.ReLU(),
            nn.ReplicationPad2d(1),
            nn.Conv2d(mid_features, mid_features, 3),
            nn.BatchNorm2d(mid_features),
            nn.ReLU(),
            nn.ReplicationPad2d(1),

            # 1x1 convolution to map features to classes
            nn.Conv2d(mid_features, num_classes, 1)
        )

    def forward(self, x: Tensor) -> Tensor:
        """Pushes a set of inputs (x) through the network.

        :param x: Input values
        :return: Module outputs
        """
        return self.layers(x)

# TODO: seperable convolutions


class UNet(nn.Module):
    r"""UNet based architecture for image auto encoding
    Performs replication padding instead of cropping for recombination of convolutional layer outputs
    """

    def __init__(self, num_in_channels: int = 3, num_out_channels: int = 3, max_features: int = 1024):
        r"""
        :param num_in_channels: Number of channels in the raw image data
        :param num_out_channels: Number of channels in the output data
        """
        super(UNet, self).__init__()
        if max_features not in [1024, 512, 256]:
            print('Max features restricted to [1024, 512, 256]')
            max_features = 1024
        features_4 = max_features // 2
        features_3 = features_4 // 2
        features_2 = features_3 // 2
        features_1 = features_2 // 2

        self.conv_block1 = UNetDownConvBlock(num_in_channels, features_1)
        self.conv_block2 = UNetDownConvBlock(features_1, features_2)
        self.conv_block3 = UNetDownConvBlock(features_2, features_3)
        self.conv_block4 = UNetDownConvBlock(features_3, features_4)

        self.deconv_block1 = UNetUpConvBlock(
            features_4, max_features, features_4)
        self.deconv_block2 = UNetUpConvBlock(
            max_features, features_4, features_3)
        self.deconv_block3 = UNetUpConvBlock(
            features_4, features_3, features_2)
        self.deconv_block4 = UNetUpConvBlock(
            features_3, features_2, features_1)

        self.output_block = UNetOutputBlock(features_2, num_out_channels)

    def forward(self, x: Tensor) -> Tensor:
        """Pushes a set of inputs (x) through the network.

        :param x: Input values
        :return: Network output Tensor
        """
        # print(f'Block: 0 Curr shape: {x.shape}')
        x, c1 = self.conv_block1(x)
        # print(f'Block: 1 Out shape: {x.shape}; features shape: {c1.shape}')
        x, c2 = self.conv_block2(x)
        # print(f'Block: 2 Out shape: {x.shape}; features shape: {c2.shape}')
        x, c3 = self.conv_block3(x)
        # print(f'Block: 3 Out shape: {x.shape}; features shape: {c3.shape}')
        x, c4 = self.conv_block4(x)
        # print(f'Block: 4 Out shape: {x.shape}; features shape: {c4.shape}')
        d1 = self.deconv_block1(x)
        # print(f'Block: 5 Out shape: {d1.shape}')
        d2 = self.deconv_block2(torch.cat((c4, d1), dim=1))
        # print(f'Block: 6 Out shape: {d2.shape}')
        d3 = self.deconv_block3(torch.cat((c3, d2), dim=1))
        # print(f'Block: 7 Out shape: {d3.shape}')
        d4 = self.deconv_block4(torch.cat((c2, d3), dim=1))
        # print(f'Block: 8 Out shape: {d4.shape}')
        out = self.output_block(torch.cat((c1, d4), dim=1))
        # print(f'Block: 9 Out shape: {out.shape}')

        return out


class UISegmentationSystem(pytorch_lightning.LightningModule):
    """
    Predict UI segments of videogame screenshots using a Convolutional Neural Network.
    This class can be used for training, validation, and prediction.
    """

    def __init__(self, hparams):
        """
        Requires args for batch size, data folder, output folder, etc.
        """
        super(UISegmentationSystem, self).__init__()
        log.info("Running __init__")
        self.hparams = hparams
        # I usually initialize like this to use it as a boolean condition elsewhere
        self.dataset = None
        # We could use any kind of network architecture, or set it via an arg flag
        self.network = UNet(
            num_in_channels=3, num_out_channels=1, max_features=self.hparams.max_features)

        self.models_dir = f"{hparams.output_dir}/model_checkpoints"
        self.images_dir = f"{hparams.output_dir}/step_images"
        os.makedirs(self.models_dir, exist_ok=True)
        os.makedirs(self.images_dir, exist_ok=True)

    def forward(self, input):
        """
        Send an input image (tensor with batch dimension) through the prediction network and return the prediction (tensor with same batch dimension)
        This is the same as the forward method of a pytorch nn module, we just have our network in a seperate file to have fewer lines here
        """
        return self.network(input)

    # ---------------------
    # TRAINING SETUP
    # ---------------------
    def configure_optimizers(self):
        """
        REQUIRED
        Adam is a pretty popular optimizer, we'll use it
        Lightning handles the zero_grad() and step() calls
        :return: optimizer to use in training 
            (optionally return list of optimizers and a list of learning rate schedulers in a tuple)
        """
        log.info("Configuring optimizer and scheduler")
        optimizer = torch.optim.Adam(
            self.network.parameters(),
            lr=self.hparams.learning_rate
        )
        scheduler = {
            'scheduler': torch.optim.lr_scheduler.ReduceLROnPlateau(patience=7),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1
        }
        return [optimizer], [scheduler]

    def prepare_data(self):
        """
        Lightning calls this once at the beginning of training / inference, it gets called once no matter how many GPUs you're using
        Doesn't return anything, just prepares / downloads data if necessary
        """
        # Resize to square and turn PIL image into torch Tensor
        training_transform = ImageTargetCompose([
            ImageTargetResize(
                (self.hparams.image_size, self.hparams.image_size)),
            ImageTargetToTensor()
        ])
        log.info('Prepped training transform')
        self.dataset = UIImageMaskDataset(
            data_dir=self.hparams.dataset, transform=training_transform, overfit_num=self.hparams.overfit_num)

        self.val_dataset = UIImageMaskDataset(
            data_dir=self.hparams.dataset, transform=training_transform, overfit_num=self.hparams.overfit_num)

        # Configure val set with 20% of full data. or do k-fold, up to you
        # Doing this here allows the transforms to be different for each more easily
        num_samples = len(self.dataset)
        log.info(f'Full Dataset loaded: len: {num_samples}')
        num_train = int(0.8 * num_samples)
        indices = torch.randperm(num_samples).tolist()
        self.dataset = torch.utils.data.Subset(
            self.dataset, indices[0:num_train])
        self.val_dataset = torch.utils.data.Subset(
            self.val_dataset, indices[num_train:])
        log.info(
            f'Train ({len(self.dataset)} samples) and Val ({len(self.val_dataset)} samples) datasets loaded')

    def train_dataloader(self):
        """
        REQUIRED. Return pytorch dataloader to be used in training loop
        Lightning passes batches to the `training_step()` function below. 
        It also handles making a distributed sampler if you're using multiple GPUs.
        """
        log.info("Fetching training dataloader")
        return torch.utils.data.DataLoader(self.dataset, batch_size=self.hparams.batch_size, drop_last=True, shuffle=True)

    def val_dataloader(self):
        """
        REQUIRED if doing val loop. Return pytorch dataloader to be used in validation loop
        Lighting passes each batch to the `validation_step()` function below.
        """
        log.info("Fetching validation dataloader")
        # Batch_size increase if using a full val set
        return torch.utils.data.DataLoader(self.val_dataset, batch_size=self.hparams.batch_size, shuffle=False)

    def loss_function(self, targets, predictions):
        """
        Used in training and validation to backpropogate into the network weights and assess how well our model is doing.
        Combines sigmoid with Binary Cross Entropy for numerical stability.
        Can include pos_weight to increase recall of underrepresented classes
        :param targets: ground truth segmentation mask
        :param predictions: output of the network
        """
        bce_loss = F.binary_cross_entropy_with_logits(predictions, targets)
        return bce_loss

    # -----------------------------
    # TRAINING LOOP
    # -----------------------------
    def training_step(self, batch, batch_number):
        """
        Lightning calls this inside the training loop
        :param batch: Lightning yields a batch from the dataloader, in our case it is a tuple of (images, targets) already batched (tensors of dimesions [Batch, Channels, Height, Width])
        :param batch_number: batch_number in current epoch
        :return: dict
            - loss -> tensor scalar [REQUIRED]
            - progress_bar -> Dict for progress bar display. Must have only tensors
            - log -> Dict of metrics to add to logger. Must have only tensors (no images, etc)
        """
        # forward pass
        images, targets = batch
        predictions = self.forward(images)
        # calculate loss
        loss_val = self.loss_function(targets, predictions)
        to_log = {'training_loss': loss_val}
        output = {
            'loss': loss_val,  # required
            'progress_bar': to_log,
            'log': to_log,
        }

        return output

    # ---------------------
    # VALIDATION LOOP
    # ---------------------

    def validation_step(self, batch, batch_number):
        """
        Called in validation loop with model in eval mode
        :param batch: Lightning yields a batch from the dataloader, in our case it is a tuple of (images, targets) already batched (tensors of dimesions [Batch, Channels, Height, Width])
        :param batch_number: Lightning tells us what batch number we're on. We'll use this to choose when to log some images
        :return: dict
            - val_loss -> tensor scalar [Useful for Early Stopping]
            - progress_bar -> Dict for progress bar display. Must have only tensors
            - log -> Dict of metrics to add to logger. Must have only tensors (no images, etc)
        """
        images, targets = batch
        predictions = self.forward(images)
        loss_val = self.loss_function(targets, predictions)
        if batch_number == 0:
            self.log_validation_images(
                images, predictions, targets, step=self.global_step)
        to_log = {'val_loss': loss_val}
        output = {
            'val_loss': loss_val,
            'progress_bar': to_log,
            'log': to_log
        }
        return output

    def log_validation_images(self, inputs, predictions, targets, step=0):
        # Turn 1 channel binary masks into 3 channel 'rgb'
        model_outputs = torch.cat(
            (predictions, predictions, predictions), dim=1)
        viz_targets = torch.cat((targets, targets, targets), dim=1)
        img_grid = torchvision.utils.make_grid(torch.cat(
            [inputs, model_outputs, viz_targets]), nrow=inputs.shape[0], padding=20, normalize=True, range=(0, 1))

        #plt.imshow(np.asarray(img_grid.detach().permute((1, 2, 0)).cpu()))
        torchvision.utils.save_image(img_grid, os.path.join(
            self.images_dir, f"step_{step}_val_batch.png"))
