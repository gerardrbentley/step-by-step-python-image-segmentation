import os
import glob
import argparse
import logging as log

import imageio

import pytorch_lightning

import ui_segmentation_system as UI_SEG


def setup_and_train(args):
    # args = Namespace(**{
    #     'dataset': './test_inputs',
    #     'output_dir': './test_outputs',
    #     'batch_size': 4,
    #     'learning_rate': 0.001,
    #     'image_size': 256,
    #     'overfit_num': 100,
    #     'seed': 4747
    # })
    ui_predictor = UI_SEG.UISegmentationSystem(args)
    trainer = pytorch_lightning.Trainer(default_save_path=args.output_dir, progress_bar_refresh_rate=50,
                                        gpus='0', profiler=True, weights_summary='top', max_epochs=5)
    trainer.fit(ui_predictor)
    log.info("Done training")

    # Depending on training length you could do this in memory by recording a list of tensors (detached!) or converted images
    if not args.no_gif:
        gif_images = []
        file_paths = glob.glob(os.path.join(args.output_dir, "step_images/*"))
        file_paths = sorted(file_paths, key=os.path.getmtime)
        log.info(file_paths)
        for step_img in file_paths:
            gif_images.append(imageio.imread(step_img))
        num_frames = len(gif_images)
        duration = num_frames * 0.1
        imageio.mimsave(os.path.join(args.output_dir, "val_outputs.gif"),
                        gif_images, duration=duration)
        imageio.imsave(os.path.join(args.output_dir,
                                    "final_output.png"), gif_images[-1])


if __name__ == "__main__":
    # ------------------------
    # 0 Arg Parsing
    # ------------------------
    log.getLogger().setLevel(log.INFO)

    parser = argparse.ArgumentParser(
        description='UI Segmentation Training')
    parser = pytorch_lightning.Trainer.add_argparse_args(parser)

    # Directories
    parser.add_argument('-d', '--dataset',
                        default=os.path.join('.', 'test_inputs'), type=str)
    parser.add_argument("-o", "--output-dir", type=str, default="./test_outputs",
                        help="where to save validation images from training")

    # Hyperparams
    parser.add_argument('-b', '--batch-size', default=4, type=int)
    parser.add_argument('-lr', '--learning-rate', default=0.001, type=float,
                        help='initial learning rate')
    parser.add_argument("--image-size", type=int, default=256,
                        help="size of training images, default is 256")

    # Training Helpers
    parser.add_argument("--overfit-num", type=int, default=0,
                        help="Set to positive number to overfit on one image, default is 0 (no overfitting)")
    parser.add_argument("--no-gif", action='store_true',
                        help="Don't make a visualization gif at the end")

    # Mostly Set and Forget
    parser.add_argument("--seed", type=int, default=4747, help="random seed")

    args = parser.parse_args()
    setup_and_train(args)
